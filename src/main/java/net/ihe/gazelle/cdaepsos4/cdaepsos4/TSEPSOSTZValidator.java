package net.ihe.gazelle.cdaepsos4.cdaepsos4;

import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        TSEPSOSTZ
  * package :   cdaepsos4
  * 
  */
public final class TSEPSOSTZValidator{


    private TSEPSOSTZValidator() {}



	/**
	* Validation of instance by a constraint :  dtr11TSEPSOSTZ
	* Time SHALL be precise to the day 
	* 
	*/
	private static boolean _validateTSEPSOSTZ_Dtr11TSEPSOSTZ(net.ihe.gazelle.cdaepsos4.TSEPSOSTZ aClass){
		return aClass.matches(aClass.getValue(), "([0-9]{8})(.*)");
		
	}				
	/**
	* Validation of instance by a constraint :  dtr13TSEPSOSTZ
	* Time SHALL include a time zone if more precise than to the day 
	* 
	*/
	private static boolean _validateTSEPSOSTZ_Dtr13TSEPSOSTZ(net.ihe.gazelle.cdaepsos4.TSEPSOSTZ aClass){
		return aClass.matches(aClass.getValue(), "(([0-9]{8,14}([+-][0-9]{1,4}))|[0-9]{8})");
		
	}				
	/**
	* Validation of instance by a constraint : dtr12TSEPSOSTZ
	* Time SHOULD be precise to the second 
	* 
	*/
	private static boolean _validateTSEPSOSTZ_Dtr12TSEPSOSTZ(net.ihe.gazelle.cdaepsos4.TSEPSOSTZ aClass){
		return aClass.matches(aClass.getValue(), "([0-9]{14})(.*)");
		
	}				

	 /**
     * Validate an instance of {TSEPSOSTZ}
     * 
     */
    public static void _validateTSEPSOSTZ(net.ihe.gazelle.cdaepsos4.TSEPSOSTZ aClass, String location, List<Notification> diagnostic) {
		executeCons_TSEPSOSTZ_Dtr11TSEPSOSTZ(aClass, location, diagnostic);
		executeCons_TSEPSOSTZ_Dtr13TSEPSOSTZ(aClass, location, diagnostic);
		executeCons_TSEPSOSTZ_Dtr12TSEPSOSTZ(aClass, location, diagnostic);
    }

	private static void executeCons_TSEPSOSTZ_Dtr11TSEPSOSTZ(net.ihe.gazelle.cdaepsos4.TSEPSOSTZ aClass,
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTSEPSOSTZ_Dtr11TSEPSOSTZ(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest(" dtr11TSEPSOSTZ");
		notif.setDescription("Time SHALL be precise to the day");
		notif.setLocation(location);
		notif.setIdentifiant("cdaepsos4-TSEPSOSTZ- dtr11TSEPSOSTZ");
		
		diagnostic.add(notif);
	}

	private static void executeCons_TSEPSOSTZ_Dtr13TSEPSOSTZ(net.ihe.gazelle.cdaepsos4.TSEPSOSTZ aClass,
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTSEPSOSTZ_Dtr13TSEPSOSTZ(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest(" dtr13TSEPSOSTZ");
		notif.setDescription("Time SHALL include a time zone if more precise than to the day");
		notif.setLocation(location);
		notif.setIdentifiant("cdaepsos4-TSEPSOSTZ- dtr13TSEPSOSTZ");
		
		diagnostic.add(notif);
	}

	private static void executeCons_TSEPSOSTZ_Dtr12TSEPSOSTZ(net.ihe.gazelle.cdaepsos4.TSEPSOSTZ aClass,
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTSEPSOSTZ_Dtr12TSEPSOSTZ(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtr12TSEPSOSTZ");
		notif.setDescription("Time SHOULD be precise to the second");
		notif.setLocation(location);
		notif.setIdentifiant("cdaepsos4-TSEPSOSTZ-dtr12TSEPSOSTZ");
		
		diagnostic.add(notif);
	}



}
