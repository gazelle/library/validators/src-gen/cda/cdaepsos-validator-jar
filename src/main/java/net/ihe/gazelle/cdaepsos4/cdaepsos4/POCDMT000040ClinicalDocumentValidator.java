package net.ihe.gazelle.cdaepsos4.cdaepsos4;

import java.util.List;

import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        POCDMT000040ClinicalDocument
  * package :   cdaepsos4
  * 
  */
public final class POCDMT000040ClinicalDocumentValidator{


    private POCDMT000040ClinicalDocumentValidator() {}



	/**
	* Validation of instance by a constraint : constraint_copytime
	* copyTime is deprecated by CDA R2
	* 
	*/
	private static boolean _validatePOCDMT000040ClinicalDocument_Constraint_copytime(net.ihe.gazelle.cdaepsos4.POCDMT000040ClinicalDocument aClass){
		return (aClass.getCopyTime() == null);
		
	}				

	 /**
     * Validate an instance of {POCDMT000040ClinicalDocument}
     * 
     */
    public static void _validatePOCDMT000040ClinicalDocument(net.ihe.gazelle.cdaepsos4.POCDMT000040ClinicalDocument aClass, String location, List<Notification> diagnostic) {
		executeCons_POCDMT000040ClinicalDocument_Constraint_copytime(aClass, location, diagnostic);
    }

	private static void executeCons_POCDMT000040ClinicalDocument_Constraint_copytime(net.ihe.gazelle.cdaepsos4.POCDMT000040ClinicalDocument aClass,
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePOCDMT000040ClinicalDocument_Constraint_copytime(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("constraint_copytime");
		notif.setDescription("copyTime is deprecated by CDA R2");
		notif.setLocation(location);
		notif.setIdentifiant("cdaepsos4-POCDMT000040ClinicalDocument-constraint_copytime");
		
		diagnostic.add(notif);
	}



}
