package net.ihe.gazelle.cdaepsos4.cdaepsos4;


import java.util.List;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



public class CDAEPSOS4PackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CDAEPSOS4PackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.cdaepsos4.POCDMT000040ClinicalDocument){
			net.ihe.gazelle.cdaepsos4.POCDMT000040ClinicalDocument aClass = ( net.ihe.gazelle.cdaepsos4.POCDMT000040ClinicalDocument)obj;
			POCDMT000040ClinicalDocumentValidator._validatePOCDMT000040ClinicalDocument(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cdaepsos4.TSEPSOSTZ){
			net.ihe.gazelle.cdaepsos4.TSEPSOSTZ aClass = ( net.ihe.gazelle.cdaepsos4.TSEPSOSTZ)obj;
			TSEPSOSTZValidator._validateTSEPSOSTZ(aClass, location, diagnostic);
		}
	
	}

}

